#!/bin/bash

#  Copyright (C) 2020-2021 Orange
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.


set -euo pipefail

export ARTIFACT_VERSION=${ARTIFACT_VERSION:-v3.16}
export URL=${URL:-https://docs.projectcalico.org/${ARTIFACT_VERSION}/manifests/calico.yaml}
export ARTIFACT=${ARTIFACT:-calico}
export GROUPID=kanod

for var in NEXUS_KANOD_USER NEXUS_KANOD_PASSWORD NEXUS_REGISTRY REPO_URL
do
    if [ -z "${!var}" ]; then
        echo "${var} must be defined"
        exit
    fi
done

# We need the real registry (without path)
# shellcheck disable=SC2001
NEXUS_REGISTRY="$(sed 's!/.*$!!' <<< "$NEXUS_REGISTRY")"

function kanod_image_sync() {
  echo "- sync of $1"
  for source in $(grep -o 'image: .*' "$1" | tr -d "\"\'" | sed -e 's/^image: //' -e "s/['\"]//g" | sort | uniq); do
    # shellcheck disable=SC2001
    target="${NEXUS_REGISTRY}/$(sed -e 's![^/]*[:.][^/]*/!!' <<< "$source")"
    if [[ "${source}" == 'kanod-registry.io/'* ]]; then
        echo "- kanod image"
        continue
    fi
    docker login --username "${NEXUS_KANOD_USER}" \
    --password-stdin "${NEXUS_REGISTRY}" <<< "${NEXUS_KANOD_PASSWORD}"
    if docker manifest inspect "${target}" &> /dev/null; then
        echo "- already present"
        continue
    fi
    docker image pull "${source}"
    docker image tag "${source}" "${target}"
    docker image push "${target}"
    echo "- done"
  done
}

function process() {
    # shellcheck disable=SC2016
    SUBST_URL=${URL//'${ARTIFACT_VERSION}'/${ARTIFACT_VERSION}}
    echo "- fetching manifest ${SUBST_URL}"
    if ! curl -L "${SUBST_URL}" -o manifest.yaml; then
        echo "Cannot fetch manifest ${SUBST_URL}"
        exit 1
    fi
    # shellcheck disable=SC2016
    kanod_image_sync manifest.yaml
}

function upload() {
    # shellcheck disable=SC2086
    mvn ${MAVEN_CLI_OPTS} deploy:deploy-file -DgroupId="${GROUPID}" -DartifactId="${ARTIFACT}" ${MAVEN_OPTS}\
      -Dversion="${ARTIFACT_VERSION}" -Dtype=yaml -Dfile=manifest.yaml \
      -DrepositoryId=kanod -Durl="${REPO_URL}"
}

process
upload
