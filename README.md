# kanod-internalize
(renamed from kanod-calico)

This project can be used to internalize 
any kind of external manifest so that the
container images it refers to are available
on the Nexus as the edited manifest.

For each internalized manifest, three variables must be defined in the CI:
* VERSION: tag version used by
  produced artefact. (eg. `3.16`)
* URL: may refer to the VERSION
  variable if it uses the syntax: `${VERSION}` (eg. `https://docs.projectcalico.org/${VERSION}/manifests/calico.yaml`)
* ARTIFACT: the produced artifact name 
  on Nexus (eg. `calico`)

Otherwise the CI expect the following variables:
* `NEXUS_REGISTRY` Docker registry name
* `REPO_URL` Maven repository URL
* `NEXUS_ADMIN_USER` for the Maven repository
* `NEXUS_ADMIN_PASSWORD`
* `NEXUS_ADMIN_USER` for the Docker registry
* `NEXUS_KANOD_PASSWORD`

